# updateGuest

This repository contains all update guest module code.

# Setup

The clone this repository and all submodules use ``git clone --recursive`` then the URL. \
If the repository has already been cloned without the submodules, ``cd`` to the repository and run ``git submodule update --init``. \
To update the submodules, ``cd`` into the submodule's directory and use ``git`` commands such as ``git pull``.

# Docker
Have Docker and Docker-Compose installed and running. \
``cd`` to repository and run ``docker-compose build`` to create/pull images and create docker containers.\
Once images and containers are created run ``docker-compose up`` to run the Update Guest Microservice.
